#pragma checksum "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicTac\TicTac\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2fdc6f300844135df1caffc9a64bd89e3dc1a468"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicTac\TicTac\Views\_ViewImports.cshtml"
using TicTac;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicTac\TicTac\Views\_ViewImports.cshtml"
using TicTac.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2fdc6f300844135df1caffc9a64bd89e3dc1a468", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"220c60ced84943470facc918dea4f0313d9a4271", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-horizontal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Jakob\Desktop\Test\Experiment-transportlead\TicTac\TicTac\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2fdc6f300844135df1caffc9a64bd89e3dc1a4683810", async() => {
                WriteLiteral("\r\n    <div id=\"divPreviewImage\">\r\n        <fieldset>\r\n            <div class=\"form-group\">\r\n                <div class=\"col-lg-2\">\r\n                    <image");
                BeginWriteAttribute("src", " src=\"", 235, "\"", 241, 0);
                EndWriteAttribute();
                WriteLiteral(" id=\"previewImage\" style=\"height:100px;width:100px;border:solid 2px dotted; float:left\" />\r\n                </div>\r\n                <div class=\"col-lg-10\" id=\"divOpponentPlayer\">\r\n                    <image");
                BeginWriteAttribute("src", " src=\"", 448, "\"", 454, 0);
                EndWriteAttribute();
                WriteLiteral(@" id=""opponentImage"" style=""height:100px;width:100px;border:solid 2px dotted; float:right;"" />
                </div>
            </div>
        </fieldset>
    </div>
    <div id=""divRegister"">
        <fieldset>
            <legend>Register</legend>
            <div class=""form-group"">
                <label for=""name"" class=""col-lg-2 control-label"">Name</label>
                <div class=""col-lg-10"">
                    <input type=""text"" class=""form-control"" id=""name"" placeholder=""Name"">
                </div>
            </div>
            <div class=""form-group"">
                <div class=""col-lg-10 col-lg-offset-2"">
                    <button type=""button"" class=""btn btn-primary"" id=""btnRegister"">Register</button>
                </div>
            </div>
        </fieldset>
        <div id=""divFindOpponentPlayer"">
            <fieldset>
                <legend>Find a player to play against!</legend>
                <div class=""form-group"">
                    <input type=""butt");
                WriteLiteral(@"on"" class=""btn btn-primary"" id=""btnFindOpponentPlayer"" value=""Find Opponent Player"" />
                </div>
            </fieldset>
        </div>
    </div>
    <div id=""divFindingOpponentPlayer"">
        <fieldset>
            <legend>Its lonely here!</legend>
            <div class=""form-group"">
                Looking for an opponent player. Waiting for someone to join!
            </div>
        </fieldset>
    </div>
    <div id=""divGameInformation"" class=""form-group"">
        <div class=""form-group"" id=""divGameInfo""></div>
        <div class=""form-group"" id=""divInfo""></div>
    </div>
    <div id=""divGame"" style=""clear:both"">
        <fieldset>
            <legend>Game On</legend>
            <div id=""divGameBoard"" style=""width:380px"">

            </div>
        </fieldset>
    </div>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"



<script type=""text/javascript"">


    //let hubUrl = '/gameHub';
    //let httpConnection = new signalR.HttpConnection(hubUrl);
    //let hubConnection = new signalR.HubConnection(httpConnection);
    var playerName = """";
    var playerImage = """";
    var hash = ""#"";
    //hubConnection.start();

    var connection = new signalR.HubConnectionBuilder().withUrl(""/gameHub"").build();

    //const connection = new signalr.hubconnectionbuilder()
    //    .withurl(""/gamehub"")
    //    .configurelogging(signalr.loglevel.information)
    //    .build();

    //connection.start().then(function () {
    //    console.log(""connected"");
    //});

    $(""#btnRegister"").click(function () {
        playerName = $('#name').val();
        playerImage = $('#previewImage').attr('src');
        var data = playerName.concat(hash);
        hubConnection.invoke('RegisterPlayer', data);
    });

    $(""#image"").change(function () {
        readURL(this);
    });

    function readURL(input)");
            WriteLiteral(@" {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(input.files[0]);
        }
    }

    function imageIsLoaded(e) {
        if (e.target.result) {
            $('#previewImage').attr('src', e.target.result);
            $(""#divPreviewImage"").show();
        }
    };

    $(""#btnFindOpponentPlayer"").click(function () {
        hubConnection.invoke('FindOpponent');
    });

    hubConnection.on('registrationComplete', data => {
        $(""#divRegister"").hide();
        $('#divOpponentPlayer').hide();
        $(""#divFindOpponentPlayer"").show();
    });

    hubConnection.on('opponentNotFound', data => {
        $('#divFindOpponentPlayer').hide();
        $('#divFindingOpponentPlayer').show();
    });

    hubConnection.on('opponentFound', (data, image) => {
        $('#divFindOpponentPlayer').hide();
        $('#divFindingOpponentPlayer').hide();
        $('#divGam");
            WriteLiteral(@"e').show();
        $('#divGameInformation').show();
        $('#divOpponentPlayer').show();
        opponentImage = image;
        $('#opponentImage').attr('src', opponentImage);
        $('#divGameInfo').html(""<br/><span><strong> Hey "" + playerName + ""! You are playing against <i>"" + data + ""</i></strong></span>"");
        for (var i = 0; i < 9; i++) {
            $(""#divGameBoard"").append(""<span class='marker' id="" + i + "" style='display:block;border:2px solid black;height:100px;width:100px;float:left;margin:10px;'>"" + i + ""</span>"");
        }
    });

    $(document).on('click', '.marker', function () {
        if ($(this).hasClass(""notAvailable"")) { //// Cell is already taken.
            return;
        }

        hubConnection.invoke('MakeAMove', $(this)[0].id); //// Cell is valid, send details to hub.
    });

    hubConnection.on('waitingForMove', data => {
        $('#divInfo').html(""<br/><span><strong> Your turn <i>"" + playerName + ""</i>! Make a winning move!</strong></span>"");");
            WriteLiteral(@"
    });

    hubConnection.on('moveMade', data => {
        if (data.Image == playerImage) {
            $(""#"" + data.ImagePosition).addClass(""notAvailable"");
            $(""#"" + data.ImagePosition).css('background-image', 'url(' + data.Image + ')');
            $('#divInfo').html(""<br/><strong>Waiting for <i>"" + data.OpponentName + ""</i> to make a move.</strong>"");
        }
        else {
            $(""#"" + data.ImagePosition).addClass(""notAvailable"");
            $(""#"" + data.ImagePosition).css('background-image', 'url(' + data.Image + ')');
            $('#divInfo').html(""<br/><strong>Waiting for <i>"" + data.OpponentName + ""</i> to make a move.</strong>"");
        }
    });

    hubConnection.on('gameOver', data => {
        $('#divGame').hide();
        $('#divInfo').html(""<br/><span><strong>Hey "" + playerName + ""! "" + data + "" </strong></span>"");
        $('#divGameBoard').html("" "");
        $('#divGameInfo').html("" "");
        $('#divOpponentPlayer').hide();
    });

    hubCo");
            WriteLiteral(@"nnection.on('opponentDisconnected', data => {
        $(""#divRegister"").hide();
        $('#divGame').hide();
        $('#divGameInfo').html("" "");
        $('#divInfo').html(""<br/><span><strong>Hey "" + playerName + ""! Your opponent disconnected or left the battle! You are the winner ! Hip Hip Hurray!!!</strong></span>"");

    });



</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
