﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTac.Models;

namespace TicTac.Hubs
{
    public class GameHub : Hub
    {
        private static readonly ConcurrentBag<Player> players = new ConcurrentBag<Player>();
        private static readonly ConcurrentBag<Game> games = new ConcurrentBag<Game>();
        private static readonly Random toss = new Random();

        public void RegisterPlayer(string nameAndImageData)
        {

            var splitData = nameAndImageData?.Split(new char[] { '#' }, StringSplitOptions.None);
            string name = splitData[0];
            string image = splitData[1];
            var player = players?.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (player == null)
            {
                player = new Player { ConnectionId = Context.ConnectionId, Name = name, IsPlaying = false, IsSearchingOpponent = false, RegisterTime = DateTime.UtcNow, Image = image };
                if (!players.Any(j => j.Name == name))
                {
                    players.Add(player);
                }
            }
            else
            {
                player.IsPlaying = false;
                player.IsSearchingOpponent = false;
            }

            this.OnRegisterationComplete(Context.ConnectionId);
        }
        public void OnRegisterationComplete(string connectionId)
        {
            //// Notify this connection id that the registration is complete.
            this.Clients.Client(connectionId).SendAsync(Constants.RegistrationComplete);
        }

        public void FindOpponent()
        {
            var player = players.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if(player == null)
            {
                return;
            }
            player.IsSearchingOpponent = true;

            var opponent = players.Where(x => x.ConnectionId != Context.ConnectionId && x.IsSearchingOpponent && !x.IsPlaying).OrderBy(x => x.RegisterTime).FirstOrDefault();

            if(opponent == null)
            {
                Clients.Client(Context.ConnectionId).SendAsync(Constants.OpponentNotFound);
                return;
            }

            player.IsPlaying = true;
            player.IsSearchingOpponent = false;

            opponent.IsPlaying = true;
            opponent.IsSearchingOpponent = false;

            Clients.Client(Context.ConnectionId).SendAsync(Constants.OpponentFound, player.Name, player.Image);

            games.Add(new Game { Player1 = player, Player2 = opponent });
        }

        public void MakeAMove(int position)
        {
            var game = games?.FirstOrDefault(x => x.Player1.ConnectionId == Context.ConnectionId || x.Player1.ConnectionId == Context.ConnectionId);

            if (game == null || game.IsOver)
            {
                return;
            }

            int symbol = 0;

            if (game.Player2.ConnectionId == Context.ConnectionId)
            {
                symbol = 1;
            }

            var player = symbol == 0 ? game.Player1 : game.Player2;

            if(player.WaitingForMove)
            {
                return;
            }

            Clients.Client(game.Player1.ConnectionId).SendAsync(Constants.MoveMade,
                new MoveInformation { OpponentName = player.Name, ImagePosition = position, Image = player.Image });

            if(game.Play(symbol, position))
            {
                Remove<Game>(games, game);
                
                Clients.Client(game.Player1.ConnectionId).SendAsync(Constants.GameOver, $"The winner is {player.Name}");
                Clients.Client(game.Player2.ConnectionId).SendAsync(Constants.GameOver, $"The winner is {player.Name}");

                player.Opponent.IsPlaying = false;

                this.Clients.Client(player.ConnectionId).SendAsync(Constants.RegistrationComplete);
                this.Clients.Client(player.Opponent.ConnectionId).SendAsync(Constants.RegistrationComplete);

                if (game.IsOver && game.IsDraw)
                {
                    Remove<Game>(games, game);

                    Clients.Client(game.Player1.ConnectionId).SendAsync(Constants.GameOver, "It's a tame draw!");
                    Clients.Client(game.Player2.ConnectionId).SendAsync(Constants.GameOver, "It's a tame draw!");

                    player.IsPlaying = false;
                    player.Opponent.IsPlaying = false;

                    this.Clients.Client(player.ConnectionId).SendAsync(Constants.RegistrationComplete);
                    this.Clients.Client(player.Opponent.ConnectionId).SendAsync(Constants.RegistrationComplete);
                }

                if (!game.IsOver)
                {
                    player.WaitingForMove = !player.WaitingForMove;
                    player.Opponent.WaitingForMove = !player.Opponent.WaitingForMove;

                    Clients.Client(player.Opponent.ConnectionId).SendAsync(Constants.WaitingForOpponent, player.Opponent.Name);
                    Clients.Client(player.ConnectionId).SendAsync(Constants.WaitingForOpponent, player.Opponent.Name);
                }

            }
        }
        private void Remove<T>(ConcurrentBag<T> players, T playerWithoutGame)
        {
            players = new ConcurrentBag<T>(players?.Except(new[] { playerWithoutGame }));
        }


    }
}
